#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include <QDataStream>

#include "reader.h"

class Client : public QObject // куобджект-для того, чтобы пользоваться всеми сигналами, слотами внутри класса
{
    Q_OBJECT
public: // конструктор обязан быть публичным
    explicit Client(QObject *parent = nullptr); // конструктор класса унаследованного от qobject

signals: // блок с сигналами (у нас он пустой)

public slots: // методы c++ которые мы можем связывать с сигналами, могут выполняться классом, при получении сигнала
    void connected(); // void-тип значения которое он возвращает(тут он ничего не возврящает) connected-действия, выполняемые после подключения клиента;
    void readyRead(); // действия, выполняемые после того как клиент что-то прислал

    void processData(QVector<equation> equations); // действия, выполняемые после того как объект класса reader считал данные с консоли, подготовил и передал.

private:
    QTcpSocket *socket; // указатель на объект класса сокет
    Reader *rd; // указатель на объект класса reader

};

#endif // CLIENT_H // как и в 1 и 2 строке, для того чтобы инклюды работали безопаснее(влияет на то, на сколько круто в проекте собирается, безопасно ли)
