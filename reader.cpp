#include "reader.h"

Reader::Reader(QObject *parent) : QThread(parent) // конструктор регистрации класса
{
    qRegisterMetaType <QVector<equation>> ("QVector<equation>");
}

void Reader::run() // вечный цикл, в котором мы создаём строкку, объект для чтения из консоли, вектор выражений , и структуру выражения
{
    forever{
        QString data;
        QTextStream s(stdin);
        QVector<equation> equations;
        struct equation eq;

        int eq_amount = s.readLine().toInt(); // вызываем реадлайн, который возвращает строчкку из консоли, и сразу её конвертирует в число и записывает количесво уравнений

        for (int i = 0; i < eq_amount; i++) // цикл по количеству этих уравнений
        {
            data = s.readLine(); // если 4 раза цикл отработает, то он считает 4 строчки

            QStringList tokens = data.split(" "); // вызывает у строки метод сплит, который вернет нам лист  из строчек, которые в исходной строке былит разделены пробелами
            eq.a = tokens.at(0);
            eq.b = tokens.at(1);
            eq.c = tokens.at(2);

            equations.push_back(eq); // структуру equacion добавляем в вектор
        }

        emit dataReady(equations); // эммитим сигнал, данные готовы, сигнал перехватываем на сервере
    }
}
