// это конструктор
#include "client.h" // в нем мы определили класс и методы, но реализации в нем нет, а тут в сср мы вставили заголовочный файл и описываем реализацию методов и связыввем заголовочный с файлом описанием инклюдом

Client::Client(QObject *parent) : QObject(parent) // определение контруктора
{
    socket = new QTcpSocket(this); // создали объект класса сокет и сохранили указатель на него(проинициализировали)

    connect(socket, SIGNAL(connected()), this, SLOT(connected())); // кьютовская фишка( сокет излучает сигнал/ на какой сигнал этого объекта мы подписались/ указатель на объект на который мы подписываемся(указываем на объект, внутри класс которого мы находимся/ слот-метод нашего объекта this, который будет выполняться, когда сигнал будет получен)

    socket->connectToHost("127.0.0.1", 9999); // метод сокета который называется подключитьск хосту

    if(!socket->isOpen()) // внутри иф вызывается метод булевый, который говорит нам открылся сокет или нет
    {
        qDebug() << "Client could not connect"; // кудебаг для быстрого вывода в консоль сообщений
    }
    else
    {
        qDebug() << "Client connected!";
    }

    rd = new Reader(this); // создали объект класса чтец, который обрабатывает консольный ввод
    connect(rd, SIGNAL(dataReady(QVector<equation>)), this, SLOT(processData(QVector<equation>))); // подписались на сигнал чтеца "данные готовы", и связываем этот сигнал с методом "обработать данные"
}

void Client::connected() // метод работает когда клиент подключился
{
    qDebug() << "Client reading greeting..."; // просто в консоли знаем что происходим
    socket->waitForReadyRead(); // ждём когда сервер ответит и что-то скажет

    qDebug() << socket->readAll(); // в консоль выводим то, что получилось в результате ридОлл, а он читает всё, что прислал сервер
    socket->write("Hello, server"); // отправляем отправляем приветствие серверу
    socket->flush(); // очиста буфера

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead())); // ответ от сервера
    rd->start();
}

void Client::processData(QVector<equation> equations) // метод получения обработанных данных с консоли и отправка их на сервер
{
    QByteArray bytesArray;
    QDataStream out(&bytesArray ,QIODevice::WriteOnly);
    out << equations; // определение оператора находится в реадер.h
    socket->write(bytesArray);
}

void Client::readyRead() // обработка ответа сервера
{
    qDebug() << "Client reading answer...";
    QVector<equation> answer;
    QDataStream in(socket->readAll()); // считываем массив байт с сервера
    in >> answer; // массив байт перегоняем в вектор структур
    for (equation eq: answer)
    {
        qDebug() << eq.a << eq.b << eq.c; // вывод корнеей для каждого квадратного уравнения
    }
}
