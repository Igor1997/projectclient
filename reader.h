#ifndef READER_H
#define READER_H

#include <QObject>
#include <QThread>
#include <QTextStream>
#include <QDebug>
#include <sstream>

struct equation // структура в которой хранится три переменых строковые
{
    QString a;
    QString b;
    QString c;
};

inline QDataStream &operator<<(QDataStream &out, const equation &e) // оператор перегрузки
{
    out << e.a << e.b << e.c;
    return out;
}

inline QDataStream &operator>>(QDataStream &in, equation &e)
{
    in >> e.a >> e.b >> e.c;
    return in;
}

class Reader: public QThread // чтец от класса КуТреад, для того чтобы работал в отдельном потоке и не мешал работе клиента
{
    Q_OBJECT
public:
    Reader(QObject * parent = 0 ); // конструктор класса чтеца

    void run(void); // в этом методе находится логика работы чтеца

signals:
    void dataReady(QVector<equation> equations); // данные готовы, передаётс/я вектор структур
};

Q_DECLARE_METATYPE(QVector<equation>) // для того что сказать кьюту Вот этот вот вектор структур запомню, чтобы мы могли передавать этот объект в сигналах


#endif // READER_H
